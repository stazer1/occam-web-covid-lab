// The main JavaScript entrypoint for this site plugin.

// Get the utility functions from Occam itself for getParents and submitForm.
import { Util } from '../../../public/js/occam/util.js';
//import {RowsCounter} from "tablefilter/src/modules/rowsCounter";

// For all such tables, initialize the table filter...
document.querySelectorAll('table.outputs').forEach( (el) => {
    // Get number of columns.
    const colCount = el.querySelectorAll('th').length;

    // Set the options for this table.
    var options = {
        base_path: "/covid-lab/assets/tablefilter/", // Look in *our* asset path
        sticky_headers: true, // The header stays at the top during page scroll
        alternate_rows: true, // Highlight alternate rows
        locale: 'en-US',      // Force, for now, the locale for dates
        rows_counter: true,   // Print the number of rows
        btn_reset: true,      // Allow 'reset' to clear filters
        status_bar: true,     // Adds the status bar
        exclude_rows: [2],    // Do not filter the 'new run' row
        col_types: [
            'none',
            'none',
            'string',
            'date',
            'date',
            'none'
        ],
        col_0: 'none',
        col_1: 'none',
        col_2: 'multiple',
        extensions: [
            { name: 'sort' }
        ]
    };

    // Also disable the 'actions' column so it isn't filtered.
    options["col_" + (colCount - 1)] = 'none';

    const tf = new TableFilter(el, options);
    tf.init();

/*
    tf.onAfterRefreshCounter = function(o, elm, tot){
        console.log('Total number of filtered rows: '+tot);
        alert('Total number of filtered rows: ' + tot);
    };
*/

    const newRunRow = el.querySelector("tr.new-run");

    // Look for "fork" (Reuse) buttons and auto-fill the associatied "Run" item.
    el.querySelectorAll('.fork').forEach( (forkButton) => {
        // Override its click event to fill in the output row
        forkButton.addEventListener('click', (event) => {
            // Don't bubble it or do whatever the browser would normally do.
            event.preventDefault();
            event.stopPropagation();

            // Copy items into the appropriate fields.
            let row = forkButton.parentNode.parentNode;
            row.querySelectorAll('td.configuration-column').forEach( (column) => {
                let p = column.querySelector('p');
                if (p) {
                    let index = p.getAttribute('data-index');

                    let field = newRunRow.querySelector(`td[data-index="${index}"] input`);
                    if (field) {
                        // Just set a simple text field
                        field.value = p.textContent;
                    }
                    else {
                        // Find out if the input is a dropdown
                        field = newRunRow.querySelector(`td[data-index="${index}"] select`);

                        // Find the dropdown option and select it
                        if (field) {
                            field.querySelector(`option[value="${p.textContent}"]`).selected = true;
                        }
                    }
                }
            });

            // Scroll the row into view at the top
            document.body.parentNode.scrollTop = document.body.parentNode.scrollTop +
                                                 newRunRow.getBoundingClientRect().top - 100;
        });
    });

    // Attach the event to any Run buttons...
    newRunRow.querySelectorAll(".run").forEach( (runButton) => {
        runButton.addEventListener('click', (event) => {
            // Don't bubble it or do whatever the browser would normally do.
            event.preventDefault();
            event.stopPropagation();

            // Submit the form asynchronously to produce the workflow. It
            // will return the job identifier.
            const form = Util.getParents(runButton, 'form', 'form')[0];

            // Craft the configuration data from the input fields.
            // The submitForm function wants an array of [key, value] tuples.
            let configuration = [];
            newRunRow.querySelectorAll("td.configuration-column input, td.configuration-column select").forEach( (input) => {
                console.log(input.getAttribute('name') + " : " + input.value);
                configuration.push([input.getAttribute('name'), input.value]);
            });

            console.log("Config is...");
            console.log(configuration);
            // Submit the form
            Util.submitForm(form, configuration, (data) => {
            })
        });
    });
});
