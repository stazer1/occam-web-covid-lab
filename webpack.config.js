const webpack = require('webpack');
const path = require('path');
const { ESBuildMinifyPlugin } = require('esbuild-loader');

/* This describes the behavior of webpack.
*/
module.exports = {
    /* The main javascript file for the application
    */
    entry: {
        "app": "./js/index.js"
    },

    /* The eventual transpiled output file.
    */
    output: {
        path: __dirname + "/public/js",
        filename: "[name].js",
        sourceMapFilename: "[name].js.map"
    },

    mode: "production",

    /* We want source maps!
    */
    devtool: "source-map",

    /* What file types to filter.
    */
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss']
    },

    /* How to load/import modules (for each file).
    */
    module: {
        rules: [
            {
                test: /\.js$/,
                include: [/js/],
                loader: "esbuild-loader",
                options: {
                    target: 'es2015'
                }
            }
        ]
    },

    /* Minimize all vendored css */
    optimization: {
        minimizer: [
            new ESBuildMinifyPlugin({
                target: 'es2015',
                css: true
            })
        ]
    }
}
